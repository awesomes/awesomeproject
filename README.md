Taller final.

Este taller se debe desarrollar en la metodologia propuesta para usar `.gitlab` en equipos de trabajo.
Se debe evidenciar participacion conjunta de todo el equipo de trabajo.
Se deben asignar issues y se deben crear Milestones.

El programa se debe ejecutar infinitamente hasta que se le indique que termine.

Debe presentar un menu de inicio, en donde se debe poder seleccionar el algoritmo a ejecutar.


Algoritmos del taller

-Ejercicios del Primer taller.
-Simulacion de un parqueadero que tiene 5 cupos disponibles para autos; en cada cupo pueden recibir 2 motos, o pueden recibir 3 bicis.
-Simulacion de un juego de loteria, donde el usuario selecciona 3 numeros y si cae alguno de los 3 gana.
-Simular el funcionamiento de un televisor digital con 3 entradas de señal.

